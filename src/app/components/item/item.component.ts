import { Component, OnInit } from '@angular/core';
import {IItem, ItemsService} from "../../services/ItemsService/items.service";
import {ActivatedRoute} from "@angular/router";
import {MessageService} from "../../services/MessageService/message.service";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  item: IItem;
  loading:boolean;

  constructor(
      private route: ActivatedRoute,
      private itemsService: ItemsService,
      private messageService: MessageService) {
  }

  ngOnInit() {
    this.loading=true;
    this.route.paramMap
        .subscribe(params=>{
          let id = +params.get('id');
          this.itemsService.getItem(id)
              .subscribe(item=>{
                  this.loading = false;
                  this.item = item;
              });
        });
    this.messageService.setSearch(false);
  }

}
