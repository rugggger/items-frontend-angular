import {
    Component,
    ElementRef, EventEmitter,
    Input,
    OnInit, Output,
    Renderer2,
    ViewChild
} from '@angular/core';
import {ItemsService, IUpdateItemValues} from "../../../services/ItemsService/items.service";

@Component({
  selector: 'editable-field',
  templateUrl: './editable-field.component.html',
  styleUrls: ['./editable-field.component.scss']
})
export class EditableFieldComponent implements OnInit {

  @Input() value:string;
  @Input() name:string;
  @Input() id:number;
  @Output('fieldUpdated') fieldUpdated = new EventEmitter();
  @ViewChild('inputEl') inputEl: ElementRef;
  @ViewChild('spanEl')  spanEl : ElementRef;
  loading:boolean = false;
  _value:string;
  constructor(
      private renderer:Renderer2,
      private itemsService:ItemsService
      ) { }

  ngOnInit() {
    this._value = this.value;
    this.renderer.setStyle(this.inputEl.nativeElement,'display','none');

  }


  focusInput(){
    this.renderer.setStyle(this.spanEl.nativeElement,'display','none');
    this.renderer.setStyle(this.inputEl.nativeElement,'display','block');
    this.inputEl.nativeElement.focus();
  }

  updateField($event){
    if (this.loading) {return;}
    const newValue = $event.target.value;
    if (newValue !== this.value) {
      const values:IUpdateItemValues={};
      values[this.name]= newValue;
      this.loading = true;
      this.itemsService.updateField(this.id, values)
            .subscribe(res => {
                this.loading = false;
                this._value = res.item[this.name];
                this.renderer.setStyle(this.spanEl.nativeElement, 'display', 'block');
                this.renderer.setStyle(this.inputEl.nativeElement, 'display', 'none');

            });
    } else {
        this.renderer.setStyle(this.spanEl.nativeElement, 'display', 'block');
        this.renderer.setStyle(this.inputEl.nativeElement, 'display', 'none');
    }

  }

}
