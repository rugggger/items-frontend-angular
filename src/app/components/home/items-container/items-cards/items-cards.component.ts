import {Component, Input, OnInit} from '@angular/core';
import {IItem} from "../../../../services/ItemsService/items.service";

@Component({
  selector: 'items-cards',
  templateUrl: './items-cards.component.html',
  styleUrls: ['./items-cards.component.scss']
})
export class ItemsCardsComponent implements OnInit {

  @Input() items: IItem[];
  constructor() { }

  ngOnInit() {
  }

}
