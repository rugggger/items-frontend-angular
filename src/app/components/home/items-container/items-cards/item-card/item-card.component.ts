import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IItem, ItemsService} from "../../../../../services/ItemsService/items.service";
import {MessageService} from "../../../../../services/MessageService/message.service";

@Component({
  selector: 'item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {

    @Input() item: IItem;
    private loading:boolean;

    constructor(
        private itemsService: ItemsService,
        private messageService: MessageService) {
    }

  ngOnInit() {
  }

  onDelete(id:number) {

      this.loading=true;
      this.itemsService.deleteItem(id)
          .subscribe(res=>{
              this.loading=false;
              if (res.success) {
                  this.messageService.deletedItem(id);
              }
          });

  }

}
