import {Component, Input, OnInit} from '@angular/core';
import {IItem} from "../../../../services/ItemsService/items.service";

@Component({
  selector: 'items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit {

    @Input() items: IItem[];
    constructor() { }

  ngOnInit() {
  }

}
