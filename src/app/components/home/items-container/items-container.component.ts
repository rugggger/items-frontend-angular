import {Component, EventEmitter, OnInit, Output , DoCheck} from '@angular/core';
import {IItem, ItemsService} from "../../../services/ItemsService/items.service";
import {IMessage, MessageService} from "../../../services/MessageService/message.service";
import {Subscription} from "rxjs";
import {FilterItemsPipe} from "../../../common/FilterItemsPipe/filter-items.pipe";
import {SortItemsPipe} from "../../../common/SortItemsPipe/sort-items.pipe";

@Component({
  selector: 'items-container',
  templateUrl: './items-container.component.html',
  styleUrls: ['./items-container.component.scss']
})
export class ItemsContainerComponent implements OnInit, DoCheck {

  @Output('itemsListUpdated') itemsListUpdated = new EventEmitter();

  items:IItem[];
  categories:string = "[]";
  q:string = "";
  ascending:boolean = true;
  view:string = "Grid";

  private subscription: Subscription;

  constructor(
      private itemsService: ItemsService,
      private filterItemsPipe: FilterItemsPipe,
      private sortItemsPipe: SortItemsPipe,
      private messageService: MessageService) {

      this.itemsService.getItems()
          .subscribe(response => {
            this.items = response.items;
          });

      this.subscription = this.messageService.getMessage()
          .subscribe((message:IMessage) => {
              switch (message.type) {
                  case 'filter':
                      this.categories = JSON.stringify(message.data);
                      break;
                  case 'q':
                      this.q = message.data;
                      break;
                  case 'toggle_sort_order':
                      this.ascending = message.data;
                      break;
                  case 'toggle_view':
                      this.view = message.data;
                      break;
                  case 'deleted_item':
                      const id = message.data;
                      this.onDeleteItem(id);
              }
          });

  }

  get filteredItems(){
      const filtered = this.filterItemsPipe.transform(this.items,this.categories,this.q);
      return this.sortItemsPipe.transform(filtered,this.ascending);
  }

  ngOnInit() {

  }


  ngDoCheck() {
      if (this.filteredItems) {
          this.itemsListUpdated.emit(this.filteredItems.length);
      }
  }

  onDeleteItem(id) {
        const index = this.items.findIndex(item => item.id === id);
        this.items.splice(index, 1);
    }


}
