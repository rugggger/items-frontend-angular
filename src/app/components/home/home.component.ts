import { Component, OnInit } from '@angular/core';
import {MessageService} from "../../services/MessageService/message.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  totalItems:number;

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.messageService.setSearch(true);
  }

  itemsListUpdated(number:number){
    this.totalItems = number;
  }

}
