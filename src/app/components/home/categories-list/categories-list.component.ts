import { Component, OnInit } from '@angular/core';
import {ICategory, ItemsService} from "../../../services/ItemsService/items.service";
import {FormArray, FormBuilder, FormControl} from "@angular/forms";
import {MessageService} from "../../../services/MessageService/message.service";

@Component({
  selector: 'categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  private formModel;
  categories: ICategory[];
  ascending:boolean = true;
  toggleItemsView: string[] = ["Grid", "List"];
  viewItemsAs:string = "Cards";

    constructor(
        private itemsService: ItemsService,
        private fb: FormBuilder,
        private messageService: MessageService) {

        itemsService.getCategories()
            .subscribe(res => {
                this.categories = res;
                this.setCheckboxControls();
            });
    }

  private setCheckboxControls(){
      const controls = this.categories.map(c=> new FormControl(false));
      this.formModel = this.fb.group({
          categories:new FormArray(controls)
      });
  }

  ngOnInit() {


  }

  filterChanged(){
    const mapped = this.formModel.value.categories.map((val,index)=>{
        if (val) {
            return this.categories[index].id;
        }
    });
    const selectedFilters = mapped.filter(val=>val!=undefined);
    this.messageService.changeFilter(selectedFilters);
  }

    toggleSortOrder(){
        this.ascending=!this.ascending;
        this.messageService.toggleSortOrder(this.ascending);

    }

  clearFilter(){
      const checkboxes = this.formModel.controls.categories.controls;
      checkboxes.forEach(c=>c.setValue(false));
      this.filterChanged();
  }

  selectionChanged($event) {
        this.messageService.toggleView($event.value);
  }

}
