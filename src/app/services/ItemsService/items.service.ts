import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

// OBSERVABLES
import {delay} from "rxjs/operators";

export interface ICategory {
  id:number;
  title:string;
  slug:string;
  items_count:number;
}
export interface IItem {
  id:number;
  title:string;
  image_url:string;
  long_desc:string;
  short_desc:string;
  thumbnail_url:string;
  updated_at:Date;
  updated_at_human:string;
  categories: ICategory[];


}

export interface IDeleteResponse {
    success:boolean;
}

export interface IUpdateItemValues {
    [values:string]:string
}
@Injectable({
  providedIn: 'root'
})
export class ItemsService {

    readonly endpoint = 'http://127.0.0.1:8000/api';

    constructor(private http: HttpClient) { }

  getItems():Observable<{items?:IItem[]}> {
      return this.http.get(`${this.endpoint}/items`);
  }

  getItem(id:number):Observable<any> {
        return this.http.get(`${this.endpoint}/items/${id}`).pipe(delay(500));
    }

  deleteItem(id:number): Observable<IDeleteResponse>{
      return this.http.delete<IDeleteResponse>(`${this.endpoint}/items/${id}`).pipe(delay(500));
  }

  getCategories():Observable<ICategory[]> {
        return this.http.get<ICategory[]>(`${this.endpoint}/categories`);
  }

  updateField(id,values:IUpdateItemValues): Observable<any> {
        return this.http.put(`${this.endpoint}/item/${id}`,values);
  }


}
