import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";


export interface IMessage {
    type:string;
    data?:any;
}
@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private subject = new Subject<any>();

  constructor() { }

  changeFilter(filter: any) {
        this.subject.next({
            type: 'filter',
            data: filter
        });
    }

  toggleSortOrder(ascending:boolean){
      this.subject.next({
            type:'toggle_sort_order',
            data: ascending
          });
  }
  changeQueryString(q: string) {
        this.subject.next({
            type: 'q',
            data: q
        });
  }

  toggleView(view: string) {
      this.subject.next({
          type: 'toggle_view',
          data: view
      });

  }

  deletedItem(id:number) {
      this.subject.next({
          type:'deleted_item',
          data:id
      });
  }

  setSearch(visible:boolean){
      this.subject.next({
          type:'set_search',
          data:visible
      });
  }

  getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
