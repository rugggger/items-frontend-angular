import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ItemComponent } from './components/item/item.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { CategoriesListComponent } from './components/home/categories-list/categories-list.component';
import { ItemsContainerComponent } from './components/home/items-container/items-container.component';
import { ItemCardComponent } from './components/home/items-container/items-cards/item-card/item-card.component';
import {HttpClientModule} from "@angular/common/http";
import {ItemsService} from "./services/ItemsService/items.service";
import { ReactiveFormsModule} from "@angular/forms";
import { FilterItemsPipe } from './common/FilterItemsPipe/filter-items.pipe';
import { SortItemsPipe } from './common/SortItemsPipe/sort-items.pipe';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { MatButtonToggleModule} from "@angular/material";
import { ItemsListComponent } from './components/home/items-container/items-list/items-list.component';
import { ItemsCardsComponent } from './components/home/items-container/items-cards/items-cards.component';
import { EditableFieldComponent } from './components/item/editable-field/editable-field.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ItemComponent,
    NotFoundComponent,
    NavBarComponent,
    CategoriesListComponent,
    ItemsContainerComponent,
    ItemCardComponent,
    FilterItemsPipe,
    SortItemsPipe,
    ItemsListComponent,
    ItemsCardsComponent,
    EditableFieldComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
      BrowserAnimationsModule,
      MatButtonToggleModule
  ],
  providers: [
      ItemsService,
      FilterItemsPipe,
      SortItemsPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
