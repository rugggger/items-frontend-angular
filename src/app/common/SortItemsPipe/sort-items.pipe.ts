import { Pipe, PipeTransform } from '@angular/core';
import {IItem} from "../../services/ItemsService/items.service";

@Pipe({
  name: 'sortItems'
})
export class SortItemsPipe implements PipeTransform {

  transform(items: IItem[], sortAscending?: any): any {

    if (sortAscending) {
      if (!items) {return null}

      return items.sort((item1,item2)=>   {
              if (item1.title < item2.title) { return -1; }
              if (item1.title > item2.title) { return 1; }
              return 0;
          } );

    } else {
        return items.sort((item1,item2)=>   {
            if (item1.title < item2.title) { return 1; }
            if (item1.title > item2.title) { return -1; }
            return 0;
        } );


    }

  }

}
