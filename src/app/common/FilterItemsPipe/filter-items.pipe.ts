import { Pipe, PipeTransform } from '@angular/core';
import {IItem} from "../../services/ItemsService/items.service";

@Pipe({
  name: 'filterItems'
})
export class FilterItemsPipe implements PipeTransform {

  transform(items: IItem[], categories?: any, q?:string): any {

    if (categories && categories!=='[]') {
      const f = JSON.parse(categories);
      items = items.filter(item=>{
        return  item.categories.some(cat=>f.some(id=>id===cat.id));
      });
    }

    if (q) {

      items = items.filter(item=>{
        const title = item.title.toLowerCase();
        return (title.indexOf(q.toLowerCase()) > -1)
      });

    }

    return items;



  }

}
