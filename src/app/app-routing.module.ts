import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {ItemComponent} from "./components/item/item.component";
import {NotFoundComponent} from "./components/not-found/not-found.component";

const routes: Routes = [
    {
      path: '',
      component: HomeComponent
    },
    {
      path: 'item/:id',
      component: ItemComponent
    },
    {
      path:'**',
      component: NotFoundComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
