import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {IMessage, MessageService} from "../services/MessageService/message.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  private formModel;
  private subscription: Subscription;
  private showSearch:boolean = true;

  constructor(
      private fb: FormBuilder,
      private messageService: MessageService) {

      this.subscription = this.messageService.getMessage().subscribe((message:IMessage)=>{
          if (message.type==='set_search') {
              this.showSearch = message.data;
          }
      });

  }

  ngOnInit() {
    this.messageService.getMessage()
    this.formModel = this.fb.group({
        search:['']
    });
  }

 updatedQueryString(){
    const q = this.formModel.get('search').value;
    this.messageService.changeQueryString(q);
 }

}
